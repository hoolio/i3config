#!/bin/bash
# suspend message display
pkill -u "$USER" -USR1 dunst

# blank the screen
# xset dpms force off

# lock the screen
i3lock --color 000000 -n

# resume message display
pkill -u "$USER" -USR2 dunst