# general install and config
## install i3
and sessions are defined in `/usr/share/xsessions/`

and you can see which one you're running with `export | grep XDG_SESSION_DESKTOP`

## install this repo in ~/.config/i3
and in that dir symlink config to config.mpb or config.ryzen etc 

## to make dark mode work.
1. install and run lxappearance, set dark mode
1. test dolphin with `dolphin --platformtheme kde`

```
export DESKTOP_SESSION=kde
export XDG_CURRENT_DESKTOP=KDE
```

try dolphin again, should be dark.

finally make this permanent add those two lines to `/etc/profile` and `/etc/environment`

## "start menu"
is done by rofi `sudo apt-get install rofi`  
and the config is installed with  
```
cd /opt
git clone https://github.com/dracula/rofi
cp rofi/theme/config1.rasi ~/.config/rofi/config.rasi
```
then because weird, move the config block before the theme block

## bar
uses py3status.  requires lm_sensors to be installed.

## notifications
uses dunst.  this came later after finding spotify and discord lock up, 
presumably whilst waiting for an (absent) notification daemon to listen to their 
events.  

code from `https://github.com/dunst-project/dunst.git` 

dependencies `agi libdbus-1-dev libx11-dev libxinerama-dev libxrandr-dev libxss-dev libglib2.0-dev libpango1.0-dev libgtk-3-dev libxdg-basedir-dev libnotify-dev`

once started can be tested with
```
notify-send "`date`" "`fortune`"
```

## clipboard monitor
`wget https://github.com/erebe/greenclip/releases/download/v4.2/greenclip && mv greenclip /opt/greenclip/greenclip`

i prefer to have the config together here, so i symlink this config to the proper location
`ln -s ~/.config/i3/greenclip.toml ~/.config/greenclip.toml`

and `chmod +x /opt/greenclip/greenclip`

## nm-applet
systray item for networking, connect/disconnect etc.

install with `sudo apt install network-manager-gnome`

## wallpaper
`sudo apt-get install feh`

## compositor
`agi picom`

## firefox tab bar hide (assuming use of tree style tabs)
as per https://medium.com/@Aenon/firefox-hide-native-tabs-and-titlebar-f0b00bdbb88b

## allow passing URLs etc from slack (etc..?)
install `kfmclient` by running `sudo apt-get install konqueror`

and if there are other issues with `tail -f ~/.xsession-errors`

## default applications, eg firefox or brave for url handling
set brave as default handler for http and https:
```
xdg-mime default brave.desktop x-scheme-handler/http
xdg-mime default brave.desktop x-scheme-handler/https
```
and check up what's what:
`cat ~/.config/mimeapps.list`

## ssh agent, in testing..
`sudo apt-get install ksshaskpass`
make sure this appears in your bashrc
`export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"`
and hit `mod+shift+a` to exec ssh-agent-start.sh

# MBP stuff
## hidpi
not sure how i got this working..

## xsslock for locking screen on lid close
agi xss-lock

## mouse speed
 sudo apt install xserver-xorg-input-synaptics                              

# Ryzen stuff

## screens
see connected screens with `xrandr --query`

## rotation and placement etc
used `arandr` gui to create the xrandr command as below  
`xrandr --output DP-0 --off --output DP-1 --mode 1920x1080 --pos 0x74 --rotate right --output HDMI-0 --off --output DP-2 --off --output DP-3 --mode 1920x1080 --pos 4920x74 --rotate left --output HDMI-1 --primary --mode 3840x2160 --pos 1080x0 --rotate normal --output DP-4 --off --output DP-5 --off`

## tray location
in the bar section of the i3 config, set to the correct display

## mouse cursor wrapping and fixing misaligned screens
https://github.com/Airblader/xedgewarp/tree/master  
for dependencies try  
`sudo apt-get install xcb libxcb-xkb-dev x11-xkb-utils libx11-xcb-dev libxkbcommon-x11-dev libxcb-randr0-dev libxcb-util-dev`  
to run use  
`xedgewarp -b -m closest -t horizontal`

## MUSIC STUFF
set hdmi audio as output source on resume

```
sudo ln -s /home/hoolio/.config/i3/music/set-hdmi-output-on-resume.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable set-hdmi-output-on-resume.service
```

multimedia keys
```
agi playerctl
```