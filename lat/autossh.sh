#!/bin/bash

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# killall autossh

## these require matching config in ~/.ssh/config
#
# this for work stuff
# ssh -f -A -L 41415:localhost:22 cli2 # not working at home
ssh -f -N -L 41415:localhost:22 cli2
ssh -D 7337 -q -C -N -f bm-00

zenity --info --text="ssh -f -N -L 41415:localhost:22 cli2"

# this socks proxy for home
# autossh -D 1337 -q -C -N -f riverbed
