#!/bin/sh

## The idea here was to hit a compbo and have a new workspace appear with some standard apps

i3-msg "exec /usr/bin/brave-browser-stable"
i3-msg "exec /usr/bin/konsole"
# i3-msg "exec --no-startup-id konsole"
# i3-msg "split horizontal, layout tabbed"
# i3-msg "exec --no-startup-id sh -c 'export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket" && /usr/bin/code'"
