#!/bin/sh
set -e
xset s off dpms 0 10 0

# suspend message display
pkill -u "$USER" -USR1 dunst

/usr/bin/i3lock --color=000000 --ignore-empty-password --show-failed-attempts --nofork
xset s off -dpms

# resume message display
pkill -u "$USER" -USR2 dunst