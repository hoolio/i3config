#!/bin/sh
xrandr --auto
xrandr --output eDP-1 --primary --mode 1920x1080 --pos 847x1440 --rotate normal --output HDMI-1 --off --output DP-1 --mode 3440x1440 --pos 0x0 --rotate normal --output DP-2 --off

# will move ALL non empty workspaces to DP1
i3-msg "[class=".*"] move workspace to output DP-1"

# run manually, will move the active workspace to laptop
# i3-msg "move workspace to output eDP-1"