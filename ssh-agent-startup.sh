#!/bin/bash
killall ssh-agent

export SSH_ASKPASS="/usr/bin/ksshaskpass"
export SSH_ASKPASS_REQUIRE=prefer
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

ssh-agent -a /run/user/1000/ssh-agent.socket

# note the key order can matter
ssh-add ~/.ssh/UTAS.key
ssh-add ~/.ssh/PANDA.key
ssh-add ~/.ssh/HOME.key